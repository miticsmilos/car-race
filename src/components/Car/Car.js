import React from 'react';
import './Car.css'

const car = (props) => (
    <li className="car" >
        <div id="card" onClick={props.carClick} className={props.isSelected ? 'cardBorderSelected' : 'cardBorder'}>
            <figure className="front"><img src={props.imageLink} alt="" /></figure>
            <figure className="back"><h3>{props.carName}</h3>
            {props.carDesc}</figure>
        </div>
    </li>
);

export default car;