import React from 'react';
import uuid from 'uuid';

import './Track.css';

const trackSegments = new Array(10).fill(0);

const Track = ({ car }) => (
    <div className='track'>
        {trackSegments.map(segment => <div className="track_segment" key={uuid()}></div>)}
        <div className="track_car" id={car.id}>
            <img className="img-fluid" src={car.image} alt={car.name} />
        </div>
    </div>
    
)

export default Track;