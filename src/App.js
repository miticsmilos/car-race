import React, { Component } from 'react';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import axios from 'axios';
import './App.css';
import Car from './components/Car/Car'
import Track from './components/Track/Track'

class App extends Component {
  constructor(props) {
    super(props);
      this.state = {
        cars: [],
        selectedCarId: '',
        selectedCars: [],
        term: ''
      }
      this.searchHandler = this.searchHandler.bind(this);
  }
  
  searchingFor = (term) => {
    return function (x) {
      return x.name.toLowerCase().includes(term.toLowerCase()) || !term ;
    }
  }

  componentDidMount () {
    axios.get('./data/data.json')
      .then(res => {
        this.setState({cars: res.data.cars});
      });   
  }

  searchHandler = (event) => {
    this.setState({term: event.target.value});
  }

  selectCar = (id) => {
    const isSelected = this.state.selectedCars.some(car => car.id === id);
    if (!isSelected) {
        const car = this.state.cars.filter(car => car.id === id);
        this.setState(prevState => ({ selectedCars: [...prevState.selectedCars, ...car]}));
    } else {
        const car = this.state.selectedCars.filter(car => car.id !== id);
        this.setState({ selectedCars: [...car]});
    }
  }

  startRace = () => {
    this.state.selectedCars.forEach(car => this.setCar(car));
  }

  setCar = (car, raceSpeed = 100) => {
    let distance = 0;
    let setIntervalID = undefined;
    let carElement = document.getElementById(car.id);
    const speedPerSecond = car.speed / 3600;

    setIntervalID = setInterval(() => {
        if( distance >= 50) {
            clearInterval(setIntervalID);
        }
        else {
            distance = distance + speedPerSecond;
        }
        carElement.style.left = `${(1.8 * distance).toFixed(2)}%`;
    }, 1000 / raceSpeed);
}
  
  render() {
    const cars = this.state.cars.filter(this.searchingFor(this.state.term)).map(car => {
      return <Car key={car.id}
              imageLink={car.image} 
              carName={car.name} 
              isSelected={this.state.selectedCars.some(selectedCar => selectedCar.id === car.id)}
              carDesc={car.description}
              carClick={() => this.selectCar(car.id)}/>
    });
    return (
      <section>
        <div>
          <form>
            <input className="searchInput" type="text" onChange={this.searchHandler} value={this.state.term}/>
            <i className="fa fa-search"></i>
          </form>
        </div>
        <div className="carList">
          <ul>
            {cars}
          </ul>
        </div>
        <div>
        {this.state.selectedCars.length > 0 && this.state.selectedCars.map(car => 
          <Track
              car={car}
              key={car.id}
              distance={this.state.distance}
          />)}
          {
            this.state.selectedCars.length > 0 && <button type="button" className="startBtn" onClick={this.startRace}>START</button>
          }
        </div>
      </section>
    );
  }
}

export default App;
